import { combineReducers } from 'redux';

const songsReducer = () => {
    return [
        { title: 'Alfa Romeu e Julieta', duration: '4:00'},
        { title: 'Amor Anónimo', duration: '4:25'},
        { title: 'Mesa para dois no Carpa', duration: '3:38'},
        { title: 'Rapsódia Gentil', duration: '3:40'}
    ];
};

const selectedSongReducer = (selectedSong=null, action) => {
    if(action.type === 'SONG_SELECTED'){
        return action.payload;
    }

    return selectedSong;
};

export default combineReducers ({
    songs: songsReducer,
    selectedSong: selectedSongReducer
});